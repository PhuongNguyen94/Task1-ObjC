//
//  UserViewController.h
//  Day1
//
//  Created by Administrator on 1/30/18.
//  Copyright © 2018 Administrator. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "People+CoreDataClass.h"

@interface UserViewController : UITableViewController<UITableViewDelegate,UITableViewDataSource,UINavigationControllerDelegate,UIImagePickerControllerDelegate>

- (void)FetchData;
@property (strong) NSMutableArray *Person;




@end
