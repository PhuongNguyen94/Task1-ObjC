//
//  UserViewController.m
//  Day1
//
//  Created by Administrator on 1/30/18.
//  Copyright © 2018 Administrator. All rights reserved.
//

#import "UserViewController.h"
#import "SimpleTableCell.h"
#import "AppDelegate.h"
#import "People+CoreDataProperties.h"




@interface UserViewController (){
    

    AppDelegate *appDelegate;
    NSManagedObjectContext *context;
    NSArray *dictionaries;
    People *people;
    
}
@end

@implementation UserViewController



- (void)viewDidLoad {
    [super viewDidLoad];
     // MARK : - Get Context
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    context = appDelegate.persistentContainer.viewContext;

 


}
-(void)viewWillAppear:(BOOL)animated{
  
    [self FetchData];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return _Person.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    
    SimpleTableCell * cell = [tableView dequeueReusableCellWithIdentifier:@"CellId" forIndexPath:indexPath];
    people = [_Person objectAtIndex:indexPath.row];
    cell.labelName.text = people.name;
    cell.labelAge.text = [NSString stringWithFormat:@"%hd",people.age];
    [self.tableView beginUpdates];
    
    return cell;

}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 150;
}

//MARK : - Get Data
- (IBAction)addButton:(id)sender {
    [self.tableView reloadData];
    [self CreatePresentItem];
  
//    UIImagePickerController *picker = [[UIImagePickerController alloc]init];
//    picker.delegate = self;
//    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
//    picker.allowsEditing = YES;
//    [self presentViewController:picker animated:YES completion:nil];
  
}
//- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
//
//    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
//    UIImageWriteToSavedPhotosAlbum(chosenImage, self, nil, nil);
//
//    [picker dismissViewControllerAnimated:YES completion:nil];
//
//}

//MARK : - Load Data
-(void)FetchData{
    NSFetchRequest *requestExamLocation = [NSFetchRequest fetchRequestWithEntityName:@"People"];
    NSArray *results = [context executeFetchRequest:requestExamLocation error:nil];
    if (results!=nil){
        _Person = [[NSMutableArray alloc]initWithArray:results];
    }
    [self.tableView reloadData];
}

-(void)CreatePresentItem{
    //MARK : - Create entity
    NSManagedObject *entityObj = [NSEntityDescription insertNewObjectForEntityForName:@"People" inManagedObjectContext:context];
  
    
    // MARK: - Use Alert to get data
    
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle: @"Warning"
                                                                              message: @"Input name and age."
                                                                       preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"name";
        textField.textColor = [UIColor blueColor];
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle = UITextBorderStyleRoundedRect;
    }];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"age";
        textField.textColor = [UIColor blueColor];
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle = UITextBorderStyleRoundedRect;
        
    }];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        NSArray * textfields = alertController.textFields;
        UITextField * namefield = textfields[0];
        UITextField *agefield = textfields[1];
        //MARK: - Save Data
        [entityObj setValue:namefield.text forKey:@"name"];
        [entityObj setValue:[NSNumber numberWithInteger:[[agefield text] integerValue]] forKey:@"age"];
        [appDelegate saveContext];
        [self FetchData];
        
      
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
}




/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [context deleteObject:[self.Person objectAtIndex:indexPath.row]];
        NSError *error  = nil ;
        if(![context save:&error]){
            NSLog(@"Can't Delete! %@ %@", error, [error localizedDescription]);
            return;
        }
        [self.Person removeObjectAtIndex:indexPath.row];
        [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
        [self FetchData];
        [self.tableView reloadData];

    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    // MARK: - Use Alert to get data
    
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle: @"Warning"
                                                                              message: @"Input name and age."
                                                                       preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"name";
        textField.textColor = [UIColor blueColor];
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle = UITextBorderStyleRoundedRect;
    }];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"age";
        textField.textColor = [UIColor blueColor];
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle = UITextBorderStyleRoundedRect;
        
    }];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        NSArray * textfields = alertController.textFields;
        UITextField * namefield = textfields[0];
        UITextField *agefield = textfields[1];
        //MARK : fectch data to edit
        NSFetchRequest *requestExamLocation = [NSFetchRequest fetchRequestWithEntityName:@"People"];
        NSArray *results = [context executeFetchRequest:requestExamLocation error:nil];
        if (results!=nil){
            _Person = [[NSMutableArray alloc]initWithArray:results];
            People*p = [_Person objectAtIndex:indexPath.row];
            [p setValue:namefield.text forKey:@"name"];
            [p setValue:[NSNumber numberWithInteger:[[agefield text] integerValue]] forKey:@"age"];
            [self.tableView reloadData];
        }
        
        
        
        
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
}

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
