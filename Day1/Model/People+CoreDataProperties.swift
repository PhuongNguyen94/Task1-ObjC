//
//  People+CoreDataProperties.swift
//  Day1
//
//  Created by Administrator on 1/31/18.
//  Copyright © 2018 Administrator. All rights reserved.
//
//

import Foundation
import CoreData


extension People {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<People> {
        return NSFetchRequest<People>(entityName: "People")
    }

    @NSManaged public var age: Int16
    @NSManaged public var image: NSData?
    @NSManaged public var name: String?

}
