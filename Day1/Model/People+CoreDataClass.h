//
//  People+CoreDataClass.h
//  Day1
//
//  Created by Administrator on 1/31/18.
//  Copyright © 2018 Administrator. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface People : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "People+CoreDataProperties.h"
