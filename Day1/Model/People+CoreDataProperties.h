//
//  People+CoreDataProperties.h
//  Day1
//
//  Created by Administrator on 1/31/18.
//  Copyright © 2018 Administrator. All rights reserved.
//
//

#import "People+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface People (CoreDataProperties)

+ (NSFetchRequest<People *> *)fetchRequest;

@property (nonatomic) int16_t age;
@property (nullable, nonatomic, retain) NSData *image;
@property (nullable, nonatomic, copy) NSString *name;
@property (nullable, nonatomic, copy) NSString *imageStr;

@end

NS_ASSUME_NONNULL_END
