//
//  People+CoreDataProperties.m
//  Day1
//
//  Created by Administrator on 1/31/18.
//  Copyright © 2018 Administrator. All rights reserved.
//
//

#import "People+CoreDataProperties.h"

@implementation People (CoreDataProperties)

+ (NSFetchRequest<People *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"People"];
}

@dynamic age;
@dynamic image;
@dynamic name;
@dynamic imageStr;

@end
