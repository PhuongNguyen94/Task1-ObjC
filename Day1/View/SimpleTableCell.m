//
//  SimpleTableCell.m
//  Day1
//
//  Created by Administrator on 1/30/18.
//  Copyright © 2018 Administrator. All rights reserved.
//

#import "SimpleTableCell.h"


@implementation SimpleTableCell
@synthesize labelName = _labelName;
@synthesize labelAge = _labelAge;
@synthesize imageView = _imageView;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
