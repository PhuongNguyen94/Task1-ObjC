//
//  SimpleTableCell.h
//  Day1
//
//  Created by Administrator on 1/30/18.
//  Copyright © 2018 Administrator. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SimpleTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *labelName;
@property (weak, nonatomic) IBOutlet UILabel *labelAge;

@end
