//
//  User.h
//  Day1
//
//  Created by Administrator on 1/29/18.
//  Copyright © 2018 Administrator. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject
{
    @public NSString *Id;
    @private NSNumber *age;
    @protected NSString *name;
}

@property (nonatomic,readwrite) NSString *Id;
@property (nonatomic,readwrite) NSNumber *age;
@property (nonatomic,readwrite) NSString *name;

@end

